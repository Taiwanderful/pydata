import os

result = os.system('jupyter notebook --generate-config')

if result == 0:
    user_folder = os.environ['USERPROFILE']
    config_file_path = f'{user_folder}/.jupyter/jupyter_notebook_config.py'
    with open(config_file_path, "r") as f:
        config_content = f.read()
        original_config = "# c.NotebookApp.notebook_dir = ''"
        pydata_dir_config = f"c.NotebookApp.notebook_dir = '{os.getcwd()}'"
        config_content.replace(original_config, pydata_dir_config)
        f.write(config_content)
    input("請按任意鍵繼續...")
else:
    input("設定失敗")